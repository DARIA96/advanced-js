async function getInfoByIP() {
    try {
        // Отримання IP адреси клієнта
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const ipAddress = ipData.ip;

        // Отримання інформації про фізичну адресу за IP
        const infoResponse = await fetch(`https://ip-api.com/json/${ipAddress}`);
        const infoData = await infoResponse.json();

        // Виведення отриманої інформації на сторінку
        document.getElementById('country').textContent = infoData.country;
        document.getElementById('region').textContent = infoData.region;
        document.getElementById('city').textContent = infoData.city;
    } catch (error) {
        console.log(error);
    }
}