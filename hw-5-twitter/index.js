class TwitterAPI {
    static getUsers() {
        return fetch('https://ajax.test-danit.com/api/json/users')
            .then(response => response.json())
    }

    static getPosts() {
        return fetch('https://ajax.test-danit.com/api/json/posts')
            .then(response => response.json())
    }
}

class Card {
    constructor(post, user) {
        this.post = post;
        this.user = user;
        this.element = this.createCardElement();
    }

    createCardElement() {
        const card = document.createElement('div');
        card.classList.add('card');

        const title = document.createElement('h2');
        title.textContent = this.post.title;

        const text = document.createElement('p');
        text.textContent = this.post.body;

        const author = document.createElement('p');
        author.classList.add('author-text')
        author.textContent = `Author ${this.user.name}, email ${this.user.email}`;

        const deleteButton = document.createElement('span');
        deleteButton.classList.add('delete-button');
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => this.deleteCard())

        card.appendChild(title);
        card.appendChild(text);
        card.appendChild(author);
        card.appendChild(deleteButton);

        return card;
    }

    deleteCard() {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {method: 'DELETE'})
            .then(response => {
                if(response.ok) {
                    this.element.remove()
                }
            })
    }

}

function renderCards(users, posts) {
    posts.forEach(post => {
        const user = users.find(user => user.id === post.userId);
        if (user) {
            const card = new Card(post, user);
            document.querySelector('#cardsContainer').appendChild(card.element);
        }
    })
}


function main() {
    TwitterAPI.getUsers().then(users => {
        TwitterAPI.getPosts().then(posts => {
            renderCards(users, posts)
        })
    })
}

main()