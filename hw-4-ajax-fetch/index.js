fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(films => {
        const movieList = document.createElement('ol');
        document.body.append(movieList);

        return Promise.all(films.map(film => {
            const liElement = document.createElement('li');
            liElement.innerHTML = `<strong>Episode ${film.id}: ${film.name}</strong>.<p><em>${film.openingCrawl}</em></p>`;
            liElement.classList.add('film-heading');
            movieList.append(liElement);

            const loadingElement = document.createElement('div');
            loadingElement.classList.add('load');
            liElement.append(loadingElement);

            return fetch('https://ajax.test-danit.com/api/swapi/people')
                .then(response => response.json())
                .then(characters => {
                    liElement.removeChild(loadingElement);

                    const charactersList = document.createElement('ul');
                    liElement.append(charactersList);

                    const heading1 = document.createElement('h4');
                    heading1.innerText = 'Characters:';
                    charactersList.append(heading1);

                    characters.forEach(character => {
                        const characterItem = document.createElement('li');
                        characterItem.innerHTML = character.name;
                        charactersList.append(characterItem);
                    });
                });
        }));
    });



