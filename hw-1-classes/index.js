class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    set setName(newName) {
        this._name = newName;
    }
    get getName() {
        return this._name;
    }
    set setAge(newAge) {
        this._age = newAge;
    }
    get getAge() {
        return this._age;
    }
    set setSalary(newSalary) {
        this._salary = newSalary;
    }
    get getSalary() {
        return this._salary;
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get getSalary() {
        return this._salary * 3;
    }
    set setLang(newLang) {
        this._lang = newLang;
    }
    get getLang() {
        return this._lang;
    }
}
let programmer1 = new Programmer('John', 29, 3000, ['JavaScript','C#', 'Python']);
let programmer2 = new Programmer('David', 33, 4500, 'PHP');

console.log(programmer1.getName);
console.log(programmer2.getLang);