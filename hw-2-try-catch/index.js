const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 7
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function isValidData(book) {
    return book.hasOwnProperty('author') &&
        book.hasOwnProperty('name') &&
        book.hasOwnProperty('price')
}

function createBookList(books) {
    const rootId = document.querySelector('#root');
    const ul = document.createElement('ul');

    books.forEach((book) => {
        try {
            if (!isValidData(book)) {
                throw new Error( `${book.author || 'Невідомий автор'}, ${book.name}, ${book.price || 'Ціна не вказана'}`);
            }
            const li = document.createElement('li');
            li.textContent = `${book.author}, ${book.name}, ${book.price}`;
            ul.appendChild(li);
        } catch (error) {
            console.error('Помилка: ' + error.message);
        }
    });
    rootId.appendChild(ul);
}

createBookList(books);